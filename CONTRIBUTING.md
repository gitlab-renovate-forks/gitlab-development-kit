# Developer Certificate of Origin + License

By contributing to GitLab Inc., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab Inc.
Except for the license granted herein to GitLab Inc. and recipients of software
distributed by GitLab Inc., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

All Documentation content that resides under the [doc/ directory](/doc) of this
repository is licensed under Creative Commons:
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Contributing to GitLab Development Kit

The source of GitLab Development Kit is
[hosted on GitLab.com](https://gitlab.com/gitlab-org/gitlab-development-kit/) and contributions are welcome!

Before submitting a merge request, see [Contributing to GitLab Development Kit](doc/contributing_to_gdk.md) to learn about coding conventions.
