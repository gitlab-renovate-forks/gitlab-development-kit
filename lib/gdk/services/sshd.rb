# frozen_string_literal: true

module GDK
  module Services
    class Sshd < Base
      def name
        'sshd'
      end

      def enabled?
        config.sshd.enabled?
      end

      def command
        if config.sshd.use_gitlab_sshd?
          %(#{config.gitlab_shell.dir}/bin/gitlab-sshd -config-dir #{config.gitlab_shell.dir})
        else
          %(#{config.sshd.bin} -e -D -f #{config.gdk_root.join('openssh', 'sshd_config')})
        end
      end
    end
  end
end
