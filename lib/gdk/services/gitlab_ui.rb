# frozen_string_literal: true

module GDK
  module Services
    class GitlabUi < Base
      def name
        'gitlab-ui'
      end

      def enabled?
        config.gitlab_ui?
      end

      def command
        %(support/exec-cd gitlab-ui yarn build --watch)
      end

      def env
        {
          NODE_ENV: 'development'
        }
      end
    end
  end
end
