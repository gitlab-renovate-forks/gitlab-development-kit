# frozen_string_literal: true

require 'fileutils'
require 'net/http'

desc 'Update your GDK'
spinner_task update: %w[
  update:gdk_bundle_install
  update:gitlab
  update:platform
  preflight-checks
  preflight-update-checks
  update:subprojects
  update:make:unlock-dependency-installers
].freeze

namespace :update do
  Support::Rake::Update.make_tasks.each do |make_task|
    desc "Run `make #{make_task.target}`"
    task "make:#{make_task.target}" do |t|
      t.skip! if make_task.skip?

      success = GDK.make(make_task.target).success?
      raise Support::Rake::TaskWithLogger::MakeError, make_task.target unless success
    end
  end

  desc 'Install gems for GDK'
  task :gdk_bundle_install do
    sh = GDK::Shellout.new(%w[bundle install], chdir: GDK.config.gdk_root).execute
    raise StandardError, 'bundle install failed to succeed' unless sh.success?
  end

  desc 'Download GraphQL schema'
  task 'graphql' do
    GDK::PackageHelper.new(
      package: :graphql_schema,
      project_id: 278964 # gitlab-org/gitlab
    ).download_package
  end

  desc 'Platform update'
  task 'platform' do
    sh = GDK::Shellout.new('support/platform-update', chdir: GDK.config.gdk_root).execute
    raise StandardError, 'support/platform-update failed to succeed' unless sh.success?
  end

  desc nil
  task 'gitlab' => %w[
    make:gitlab-git-pull
    make:gitlab-setup
    make:postgresql
  ]

  desc nil
  multitask 'subprojects' => %w[
    gitlab-db-migrate
    update:graphql
    make:gitlab-translations-unlock
    make:gitaly-update
    make:ensure-databases-setup
    make:gitlab-shell-update
    make:gitlab-http-router-update
    make:gitlab-topology-service-update
    make:docs-gitlab-com-update
    make:gitlab-elasticsearch-indexer-update
    make:gitlab-k8s-agent-update
    make:gitlab-pages-update
    make:gitlab-ui-update
    make:gitlab-workhorse-update
    make:gitlab-zoekt-indexer-update
    make:gitlab-ai-gateway-update
    make:grafana-update
    make:jaeger-update
    make:object-storage-update
    make:pgvector-update
    make:zoekt-update
    make:openbao-update
    make:gitlab-runner-update
    make:duo-workflow-service-update
    make:siphon-update
    make:nats-update
  ]
end
