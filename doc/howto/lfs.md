# Git LFS

By default, Git LFS over HTTP is available in GDK without any extra configuration.

If you need to test Git LFS over SSH in GDK, you need run additional commands:

1. `gdk config set gitlab_shell.lfs.pure_ssh_protocol_enabled true`
1. `gdk reconfigure`
1. `gdk restart sshd`

These steps update the `lfs` section of your `<GDK_ROOT>/gitlab-shell/config.yml`
file to set the `pure_ssh_protocol` value to `true`:

```yaml
lfs:
  # See https://gitlab.com/groups/gitlab-org/-/epics/11872 for context
  pure_ssh_protocol: true
```

By default, the `git` and `git-lfs` clients do not display which protocol they are using. To check that you're using SSH:

1. Set the `GIT_TRACE` environment variable to `1`.
1. Perform a `git` operation on a repository with LFS enabled that is hosted in your GDK and you should see references to `pure SSH connection successful` in the output,
   which tells you the SSH protocol is being used.

   ```shell
   $ export GIT_TRACE=1
   $ git clone ssh://git@gdk.test:2222/root/lfs-project.git 2>&1 | grep 'pure SSH connection successful'
   16:40:36.545395 trace git-lfs: pure SSH connection successful
   16:40:36.735093 trace git-lfs: pure SSH connection successful
   ```
