# Migrate to `mise` for dependency management

> [!important]
> **Update (February 26, 2025):** We've extended the migration due date from March 1st to April 1st to give everyone more time to transition.
>
> Starting **April 1st**, `mise` becomes the default tool version manager for GDK. To avoid any disruptions, please migrate
> from `asdf` to `mise` as soon as possible.

GDK now supports `mise` as an alternative to `asdf` for managing dependencies. **Note:** You can use either one, but please avoid using both at the same time.

To migrate to `mise`, please follow these steps from your GDK directory:

1. Run the migration command:

   ```shell
   bundle exec rake mise:migrate
   ```

   This Rake task will automatically:

   - Opt out of `asdf`
   - Enable `mise` support
   - Install the new local hooks and dependencies
   - Re-bootstrap GDK

1. Reconfigure and update GDK.

   ```shell
   gdk reconfigure && gdk update
   ```

1. [Uninstall asdf](https://asdf-vm.com/manage/core.html#uninstall) if you're not using it outside of GDK.

## Troubleshooting

If you encounter problems with mise, see [the troubleshooting page](../troubleshooting/mise.md).
